const express = require('express');
const axios  = require('axios');

const app = express();

app.get('/' , (req , res)=>{
    res.send("8082 home");
})

const slackToken = 'xoxb-2765903461381-2765956304357-awlpGiRDZt8Fzz67WI3YXyio';
// xoxb-2765903461381-2765956304357-awlpGiRDZt8Fzz67WI3YXyio

app.get('/v1' , async(req , res)=>{
    res.send("8082 v1");
})

app.get('/v1/v2' , (req , res)=>{
    res.send("8082 v1/v2");
})
function sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
}


async function  sendMessageToSlack(mess){
    const url = 'https://slack.com/api/chat.postMessage';
    const resp = await axios.post(url, {
        channel: '#test',
            text: `${mess}`
    }, { headers: { authorization: `Bearer ${slackToken}` } });
    // console.log('dffsfsdfsfsfsfsdffsdf' , resp.data);

    return resp.data;
}

app.get('/slack-integration' , async(req , res)=>{
        try {
            const data = await axios.get('http://localhost:8081' , {timeout: 200});
            console.log('I am in try block' + data);    
        } catch (error) {
            // console.log(error);

            const resp = await sendMessageToSlack(error.code);

            console.log('Done', resp);
            // console.log(error.code); // ECONNABORTED => timeout, ECONNREFUSED => server down
            //error.isAxiosError
            console.log('I am in catch block');
        }
        
        await sleep(2000);

    res.send("8082 slack-integration");
})

app.listen('8082' , ()=>{
    console.log('server connected 8082')
})